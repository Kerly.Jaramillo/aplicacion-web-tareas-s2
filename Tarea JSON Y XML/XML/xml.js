leerXML();
var fileXML = leerXML("xml.xml");    //leer el documento XML
const listaXML = fileXML.getElementsByTagName("estudiante");   //comprueba cuantos objetos tiene el XML de la rama estudiante

function leerXML(directorio) {
    try {
        let xmlhttp = new window.XMLHttpRequest();
        xmlhttp.open("GET", directorio, false);
        xmlhttp.send(null);
        fileXML = xmlhttp.responseXML.documentElement;
        return fileXML;
    }
    catch (e) {
        return undefined;
    }
}


for(let i=0 ; i<listaXML.length ; i++){
    const elemento = document.querySelector('.box-estudent');

    const id =  fileXML.getElementsByTagName("id")[i].childNodes[0].nodeValue;
    const apellidos =  fileXML.getElementsByTagName("apellidos")[i].childNodes[0].nodeValue;
    const nombres =  fileXML.getElementsByTagName("nombres")[i].childNodes[0].nodeValue;
    

    const plantilla = document.createElement('div');
    plantilla.innerHTML = `<button class="name-estudiante xml" data-id="${id}">${nombres} ${apellidos}</button>`;
    elemento.appendChild(plantilla);
}


const estudiantes = document.querySelectorAll('.name-estudiante');     //botones de cada nombre de alumno

estudiantes.forEach((estudiante)=>{
    estudiante.addEventListener('click', (nombre)=>{
        const viewDetalle  = document.querySelector('.group-infor');
        let idElement = nombre.target.getAttribute('data-id');

        for(let i=0 ; i<listaXML.length ; i++){        
            const id =  fileXML.getElementsByTagName("id")[i].childNodes[0].nodeValue;
            const apellidos =  fileXML.getElementsByTagName("apellidos")[i].childNodes[0].nodeValue;
            const nombres =  fileXML.getElementsByTagName("nombres")[i].childNodes[0].nodeValue;
            const semestre =  fileXML.getElementsByTagName("semestre")[i].childNodes[0].nodeValue;
            const paralelo =  fileXML.getElementsByTagName("paralelo")[i].childNodes[0].nodeValue;
            const direccion =  fileXML.getElementsByTagName("direccion")[i].childNodes[0].nodeValue;
            const telefono =  fileXML.getElementsByTagName("telefono")[i].childNodes[0].nodeValue;
            const correo =  fileXML.getElementsByTagName("correo")[i].childNodes[0].nodeValue;

            if(id == idElement){
                viewDetalle.innerHTML = `
                <p>Apellidos: <span>${apellidos}</span></p>
                <p>Nombres: <span>${nombres}</span></p>
                <p>Semestre: <span>${semestre}</span></p>
                <p>Paralelo: <span>${paralelo}</span></p>
                <p>Dirección: <span>${direccion}</span></p>
                <p>Teléfono: <span>${telefono}</span></p>
                <p>Correo Electrónico: <span>${correo}</span></p>
            `;
            }
        }
    })
})