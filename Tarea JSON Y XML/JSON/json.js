const studenJson = [
    {
        "id": 1, 
        "apellidos": "García Bazurto",
        "nombres": "José Geovanny ",
        "semestre": "Décimo",
        "paralelo": "A",
        "direccion": "La paz",
        "telefono": 0922156871,
        "correo": "josebazurto@gmail.com"
    },
    {
        "id": 2, 
        "apellidos": "Cevallo Quijije",
        "nombres": "Jeyleen ",
        "semestre": "noveno",
        "paralelo": "A",
        "direccion": "Montecristi",
        "telefono": 0928234481,
        "correo": "jeyleens3@gmail.com"
    },
    {
        "id": 3, 
        "apellidos": "Salmon Quijije",
        "nombres": "Leslie Dayana",
        "semestre": "Octavo",
        "paralelo": "B",
        "direccion": "Montecristi",
        "telefono": 0999355381,
        "correo": "ldayanas@gmail.com"
    },
    {
        "id": 4,
        "apellidos": "Vélez Zamora",
        "nombres": "Mónica Cruz",
        "semestre": "séptimo",
        "paralelo": "B",
        "direccion": "Cuba",
        "telefono": 0928388111,
        "correo": "monicazv1@gmail.com"
    },
    {
        "id": 5,
        "apellidos": "Cardenas Cevallos",
        "nombres": "Rocio",
        "semestre": "Sexto",
        "paralelo": "A",
        "direccion": "Los esteros",
        "telefono": 0928385551,
        "correo": "rocio167@gmail.com"
    },
    {
        "id": 6,
        "apellidos": "Moreira Zamora",
        "nombres": "Miguel angel",
        "semestre": "Quinto",
        "paralelo": "B",
        "direccion": "La pradera",
        "telefono": 0999312358,
        "correo": "angel456@gmail.com"
    },
    {
        "id": 7,
        "apellidos": "Zamora",
        "nombres": "Rosa Carolina",
        "semestre": "Cuarto",
        "paralelo": "B",
        "direccion": "Manta Bich",
        "telefono": 0959776487,
        "correo": "rosa123@gmail.com"
    },
    {
        "id": 8,
        "apellidos": "Jaramillo Zamora ",
        "nombres": "Kerly Elizabeth ",
        "semestre": "Segundo",
        "paralelo": "A",
        "direccion": "El Mirador ",
        "telefono": 0991921191,
        "correo": "kerlyjaramillo@gmail.com"
    },
    {
        "id": 9,
        "apellidos": "Velez Moreira",
        "nombres": "Erick Omar",
        "semestre": "tercero",
        "paralelo": "A",
        "direccion": "Los esteros",
        "telefono": 0928613481,
        "correo": "erickvm@gmail.com"
    },
    {
        "id": 10,
        "apellidos": "Zambrano Castillo",
        "nombres": "Juan Alberto ",
        "semestre": "Tercero",
        "paralelo": "A",
        "direccion": "San Agustín",
        "telefono": 0993565397,
        "correo": "Juan@gmail.com"
    }
  ]
  
  studenJson.forEach((estudiante)=>{
    const elemento = document.querySelector('.box-estudent');
    const plantilla = document.createElement('div');
    plantilla.innerHTML = `<button class="name-estudiante" data-id="${estudiante.id}">${estudiante.apellidos} ${estudiante.nombres } </button>`;
    elemento.appendChild(plantilla);
  })
  
  
  const estudiantes = document.querySelectorAll('.name-estudiante');
  
  estudiantes.forEach((estudiante)=>{
    estudiante.addEventListener('click', (nombre)=>{
        let id = nombre.target.getAttribute('data-id');
        studenJson.forEach((estudiante)=>{
            if(id == estudiante.id){
                const viewDetalle  = document.querySelector('.group-infor');
                viewDetalle.innerHTML = `
                    <p>Apellidos: <span>${estudiante.apellidos}</span></p>
                    <p>Nombres: <span>${estudiante.nombres}</span></p>
                    <p>Semestre: <span>${estudiante.semestre}</span></p>
                    <p>Paralelo: <span>${estudiante.paralelo}</span></p>
                    <p>Dirección: <span>${estudiante.direccion}</span></p>
                    <p>Teléfono: <span>${estudiante.telefono}</span></p>
                    <p>Correo Electrónico: <span>${estudiante.correo}</span></p>
                `;
            }
        })
    })
  })